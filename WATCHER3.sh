while true; do
    echo "something happened"
    filename=$(inotifywait -r -q --format '%f' /data/samba/allaccess/INPUT/RNA_SEQ/)
    [[ $filename != *.yaml ]] &&  echo "something happened on path $filename"
    [[ $filename == *.yaml ]] &&  echo "`Rscript RNAseqMaster_analysis.R $filename`"
done

#inotifywait -r -q -t 0  --format '%f' /data/samba/allaccess/INPUT/RNA_SEQ/ | while read FILE
#do 
# echo "something happened on path $FILE"
# echo "`/bin/sh ./PT2_EXECUTE.sh $FILE`"
#echo "`Rscript RNAseqMaster_analysis.R $FILE`"

#done
