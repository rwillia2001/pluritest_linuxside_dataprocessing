# Pluritest Ubuntu linux server side data processing #

# The Count #
![COunt_download.jpg](https://bitbucket.org/repo/B9eG9G/images/3312883803-COunt_download.jpg)

# Filesystem Layout #
![PT2_ubuntu_server_filesystem_layout.PNG](https://bitbucket.org/repo/B9eG9G/images/2402850825-PT2_ubuntu_server_filesystem_layout.PNG)

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

[* An attempt was made to follow the Google R Style guide](https://google.github.io/styleguide/Rguide.xml)

[* A git/Bitbucket enabled R-project in Rstudio was used to develop this workflow ](https://nicercode.github.io/blog/2013-05-17-organising-my-project/)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Set up and Configuration ###
* [On Amazon.com AWS substantiate an instance of Ubuntu 14.04 server ](ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-20160714 (ami-d732f0b7))
* On Amazon.com AWS substantiate an instance of Ubuntu 14.04 server (ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-20160714 (ami-d732f0b7))
* Add 50GB EBS storage volume (/data) mounted on / of server. Provision permanent IP address to Ubuntu instance.
* [Install and set up samba windows file sharing with the allccess drive at this location: /data/samba/AllAccess/](https://www.liberiangeek.net/2015/01/install-configure-samba-ubuntu-14-10/). Adjust security groups accordingly.
* This folder is accessible for read/write by an Windows server substatiated in the same account and availability zone (us-west-2).
* Using apt-get package manager install r-base (here R-3.0.2) , fastqc, git, vncserver (http://www.brianlinkletter.com/how-to-run-gui-applications-xfce-on-an-amazon-aws-cloud-server-instance/). 
* 
* Outside of the package manager get a copy of rstudio and salmon (https://github.com/COMBINE-lab/salmon/releases  ; http://salmon.readthedocs.io/en/latest/building.html#installation) and install. 
* 
* For the required R system call to salmon, salmon is placed directly into /bin with all its libs.  Install RStudio (wget http://download1.rstudio.org/rstudio-0.98.1062-amd64.deb; rstudio: http://www.thertrader.com/2014/09/22/installing-rrstudio-on-ubuntu-14-04/). This linux rstudio version, though functional for what we needed, had some none-standard behaviour and issues with git. RStudio will do the local commit to .git, but gave errors for push/pull to remote git@bitbucket. Hence, this was done manually from the command line.

# Ubuntu Server with RStudio GUI enabled by VNC #
![AWSUbuntuServer_RstudioGUI_withVNC.PNG](https://bitbucket.org/repo/B9eG9G/images/3183556865-AWSUbuntuServer_RstudioGUI_withVNC.PNG)

* 
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* For technical questions contact me on twitter: @rwillia2001
* Other community or team contact