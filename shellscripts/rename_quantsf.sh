

find . -name 'quant.sf' \
  -execdir bash -c 'old="$1"; new="$(cd ..; basename -- "$PWD").${old##*.}"; mv "$old" "$new"' - {} \;
